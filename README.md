BoneScript SocketIO Example
===========================

This is an example of how to use Socket.IO and BoneScript to control a BeagleBone Black from a remote device. You'll be able to control the brightness of LED's from a mobile device (cell phone, tablet). The code uses Node.js as the web server, Socket.IO for communication between the web page and the BeagleBone Black, and Mobile JQuery for the web page lay-out.


Getting Started
---------------
What you'll need:
- 3 LED's (Red, Green, Yellow)
- 3 330 Ohm resistors
- Breadboard
- Jumper wires

Put these parts together following the diagram in the .png file. This example uses PWM pins P9_14 ,P9_16 and P8_19 so we can dim the LED's. 


Installation
------------
Make sure you have an Internet connection through a network cable or Wifi. Connect to the BeagleBone Black by SSH. Set the current time just to be sure:

````sh
/usr/bin/ntpdate -b -s -u pool.ntp.org
````

Install Socket.IO:

````sh
cd /var/lib/cloud9
npm install socket.io
````

Download files from GitHub to the BeagleBone Black:

````sh
cd /var/lib/cloud9
git clone git://github.com/mikehumphrey/BoneScript-SocketIO
````

Update Bonescript.io to address the asynchronis PWM error. (Version 1.3.10 at the time of this writing)

````sh
cd ~
npm install -g bonescript
````

Go to the Cloud9 IDE on the BeagleBone Black, open HtmlLedSimple.js and click the RUN button. In the console you'll see the IP address of the BeagleBone Black on your network. Enter this IP address in a web browser on your cell phone or other device. 


More info
---------
Visit Logic Supply to buy the BeagleBone Black and accessories at http://www.logicsupply.com/categories/beaglebone

Written by Roland Groeneveld for LGX / Logic Supply.

MIT License. All text above must be included in any redistribution.
